package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type Post struct {
	Id     int    `json:"id"`
	UserId int    `json:"userId"`
	Title  string `json:"title"`
	Body   string `json:"body"`
}

func main() {
	post01, err := GetPost(2)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(post01)

	fmt.Println("----------------------------------------------------------------------------------------")

	allPosts, err := GetPosts()

	if err != nil {
		log.Fatal(err)
	}

	for _, po := range allPosts {
		fmt.Println(po)
		fmt.Println("----------------------------------------------------------------")
	}
}

func GetPost(id int) (*Post, error) {
	url := fmt.Sprintf("https://jsonplaceholder.typicode.com/posts/%d", id)

	respuesta, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	defer respuesta.Body.Close()

	post := &Post{}

	err = json.NewDecoder(respuesta.Body).Decode(post)

	if err != nil {
		return nil, err
	}

	return post, nil
}

func GetPosts() ([]*Post, error) {
	url := "https://jsonplaceholder.typicode.com/posts/"
	posts := []*Post{}
	respuesta, err := http.Get(url)

	if err != nil {
		return posts, err
	}

	defer respuesta.Body.Close()

	err = json.NewDecoder(respuesta.Body).Decode(&posts)

	if err != nil {
		return posts, err
	}

	return posts, nil

}
